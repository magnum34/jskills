import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './users.routes';


export default angular.module('jskillsApp.users', [uiRouter])
  .config(routing)
  .component('user', {
    template: require('./details.html'),
    controller: ['$scope','$window',function($scope,$window){
      this.user = this.data;
      if(this.user.star){
        this.class_star = 'glyphicon-star';
      }else{
        this.class_star = 'glyphicon-star-empty';
      }
      this.star = function(checked){
        checked.star = !checked.star;
        if(checked.star){
          this.class_star = 'glyphicon-star';
        }else{
          this.class_star = 'glyphicon-star-empty';
        }
        
      };
      this.delete = function(user){
        
        if($window.confirm('Are you sure user remove?') === true) {
             //var index = $resolve.users.indexOf(user)
             //$resolve.users.splice(index, 1);
        }
      };
    }],
    bindings: {
      data: '<'
    }
  })
  .name;
