'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider
  .state('user', {
    url: '/users/:id',
    resolve: {
      user: ($http) => {
        return $http.get('/api/users/123')
          .then(response => {
            return response.data;
          });
      }
    },
    template: `<div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="page-header">User: {{ $resolve.user.first_name }} {{ $resolve.user.last_name }}</h1>
                  </div>
                  <div class="col-lg-8">
                    <user data="$resolve.user"></user>
                  </div>
                </div>
              </div>`
  })
.state('userList', {
  url: '/users',
  resolve: {
    users: ($http) => {
      return $http.get('/api/users/')
        .then(response => {
          return response.data;
        });
    }
  },
  controller: ['$scope', '$window', 'users', function ($scope, $window, users) {
    users.forEach(function (element) {
      if (element.star) {
        element.class_star = 'glyphicon-star';
      } else {
        element.class_star = 'glyphicon-star-empty';
      }
    }, this);

    $scope.star = function (checked) {
      checked.star = !checked.star;
      if (checked.star) {
        checked.class_star = 'glyphicon-star';
      } else {
        checked.class_star = 'glyphicon-star-empty';
      }

    };
    $scope.delete = function (user) {

      if ($window.confirm('Are you sure user remove?') === true) {
        var index = users.indexOf(user)
        users.splice(index, 1);
      }
    };
  }],

    template: require('./list.html')
  });
}
